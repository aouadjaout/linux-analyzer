.PHONY: analyzer clean

all: analyzer

analyzer:
	opam exec -- dune build -p linux-analyzer

clean:
	opam exec -- dune clean
