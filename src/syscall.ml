open Mopsa
open Sig.Abstraction.Stateless
open C.Ast
open Ast

module Domain =
struct

  include GenStatelessDomainId
      (struct
        let name = "linux.syscall"
      end)

  let checks = []

  let () = C.Common.Builtins.add_builtin_functions [
      "syscall"
    ]

  let eval_syscall sysno args range man flow =
    match c_expr_to_z sysno flow with
    | None ->
      panic "syscall: unable to handle non-constant syscall number '%a'"
        pp_expr sysno

    | Some n ->
      let name =
        if Z.equal n (Z.of_int 430) then "fsopen"
        else
          panic "syscall %a not supported" Z.pp_print n
      in
      let fname = "__do_sys" ^ name in
      let linux = get_linux_program flow in
      let flow = set_c_program linux.kernel flow in
      let f = find_c_fundec_by_name fname flow in
      man.eval (mk_c_call f args range) flow >>$ fun ret flow ->
      set_c_program linux.user flow |>
      Eval.singleton ret

  let init prog man flow = flow

  let exec stmt man flow = None

  let eval expr man flow =
    match ekind expr with
    | E_c_builtin_call("syscall", sysno :: args) ->
      eval_syscall sysno args expr.erange man flow |>
      Option.some

    | _ -> None

  let ask query man flow = None

  let print_expr man flow pr expr = ()

end

let () = register_stateless_domain (module Domain)
