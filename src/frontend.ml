open Mopsa
open C.Ast
open Ast


let debug fmt = Debug.debug ~channel:"linux.frontend" fmt

(************)
(** Parsing *)
(************)

let parse files =
  (* Get the kernel db and the userland source file *)
  let kernel_db, user_source =
    match files with
    | [f1; f2] ->
      if Filename.extension f1 = ".db" && Filename.extension f2 = ".c" then f1, f2 else
      if Filename.extension f2 = ".db" && Filename.extension f1 = ".c" then f2, f1
      else panic "usage: linux-analyzer <db> <userland>"
    | _ ->
      panic "usage: linux-analyzer <db> <userland>"
  in
  
  (* Parse the userland program. Unset the -make-target since it is useful for the kernel only. *)
  debug "parsing the userland program";
  let target = !C.Frontend.opt_make_target in
  C.Frontend.opt_make_target := "";
  let user = C.Frontend.parse_program [user_source] in
  let user = match user.prog_kind with C_program p -> p | _ -> assert false in

  (* Parse the kernel db file. Restore the make target, disable libc stubs and
   * keep static functions (e.g. syscall are static functions). *)
  debug "parsing the kernel";
  C.Frontend.opt_make_target := target;
  C.Frontend.opt_without_libc := true;
  C.Frontend.opt_keep_static := true;
  let kernel = C.Frontend.parse_program [kernel_db] ~min_uid:1000 in
  let kernel = match kernel.prog_kind with C_program p -> p | _ -> assert false in

  (* Create the program *)
  { prog_kind = P_linux { kernel; user };
    prog_range = mk_program_range files }

let () = register_frontend {
    lang = "linux";
    parse = parse;
  }
