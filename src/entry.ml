open Mopsa
open Sig.Abstraction.Stateless
open Ast

module Domain =
struct

  include GenStatelessDomainId
      (struct
        let name = "linux.entry"
      end)

  let checks = []

  let exec_program linux range man flow =
    (* TODO: Start by executing registered initcalls of the kernel *)
    (* Execute the user program *)
    let user_prog =
      { prog_kind = C.Ast.C_program linux.user;
        prog_range = range }
    in
    C.Ast.set_c_program linux.user flow |>
    man.exec (mk_stmt (S_program(user_prog, None)) range)

  let init prog man flow =
    match prog.prog_kind with
    | P_linux linux ->
      set_linux_program linux flow |>
      C.Ast.set_c_target_info !C.Frontend.target_info

    | _ -> flow

  let exec stmt man flow =
    match skind stmt with
    | S_program({prog_kind = P_linux linux}, None) ->
      exec_program linux stmt.srange man flow |>
      Option.some

    | _ -> None

  let eval expr man flow = None

  let ask query man flow = None

  let print_expr man flow pr expr = ()

end

let () = register_stateless_domain (module Domain)
