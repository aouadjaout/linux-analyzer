open Mopsa
open C.Ast

(*******************)
(** Linux programs *)
(*******************)

type linux_program = {
  kernel: c_program;
  user: c_program;
}

type prog_kind += P_linux of linux_program

let () = register_program {
    print = (fun next fmt p ->
        match p.prog_kind with
        | P_linux _ -> Format.pp_print_string fmt "<linux>"
        | _ -> next fmt p
      );
    compare = (fun next p1 p2 ->
        match p1.prog_kind, p2.prog_kind with
        | P_linux _, P_linux _ -> 0
        | _ -> next p1 p2
      );
  }

module LinuxProgramCtx = GenContextKey
  (struct
    type 'a t = linux_program
    let print pp fmt t = Format.pp_print_string fmt "<linux>"
  end)

let linux_program_ctx = LinuxProgramCtx.key

let set_linux_program p flow =
  let ctx = Flow.get_ctx flow in
  let ctx = add_ctx linux_program_ctx p ctx in
  Flow.set_ctx ctx flow

let get_linux_program flow =
  let ctx = Flow.get_ctx flow in
  find_ctx linux_program_ctx ctx
